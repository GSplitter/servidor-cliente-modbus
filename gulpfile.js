var gulp  = require('gulp');
var gulpif = require('gulp-if');
var concat = require('gulp-concat');
var less = require('gulp-less');
var uglify = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');
var sourcemaps = require('gulp-sourcemaps');
var htmlmin = require('gulp-htmlmin');
var jsonminify = require('gulp-jsonminify');
var development = true;

// Operação padrão: chame 'gulp' no terminal
gulp.task('init', ['build-app', 'build-html', 'build-less', 'build-json']);

// Operação padrão: chame 'gulp' no terminal
gulp.task('default', ['init', 'watch']);

// Operação de produção
gulp.task('deploy', ['dev-false', 'init'], function(){
	development = true;
});
gulp.task('dev-false', function(){
	development = false;
});

// Caminho de destino
var dest = './public/dist/';

// Constroi 1 único arquivo app.js
gulp.task('build-app', function() {
	return gulp.src('./src/**/*.js')
	.pipe(gulpif(development, sourcemaps.init()))
	.pipe(concat('app.js'))
	.pipe(uglify())
	.pipe(gulpif(development, sourcemaps.write()))
	.pipe(gulp.dest(dest + 'js'));
});

// Constroi 1 único arquivo .less
gulp.task('build-less', function(){
	return gulp.src('./src/**/*.less')
	.pipe(gulpif(development, sourcemaps.init()))
	.pipe(less())
	.pipe(concat('styles.css'))
	.pipe(uglifycss())
	.pipe(gulpif(development, sourcemaps.write()))
	.pipe(gulp.dest(dest + 'css'));
});

// Realoca arquivos html
gulp.task('build-html', function(){
	return gulp.src('./src/**/*.html')
	.pipe(gulpif(!development, htmlmin({collapseWhitespace: true})))
	.pipe(gulp.dest(dest + 'html'));
});

// Realoca arquivos json
gulp.task('build-json', function(){
	return gulp.src('./src/**/*.json')
	.pipe(gulpif(!development, jsonminify()))
	.pipe(gulp.dest(dest + 'html'));
});

// Observa por mudanças nos arquivos
gulp.task('watch', function() {
	gulp.watch('./src/app/**/*.js', ['build-app']);
	gulp.watch('./src/**/*.less', ['build-less']);
	gulp.watch('./src/**/*.html', ['build-html']);
	gulp.watch('./src/**/*.json', ['build-json']);
});
