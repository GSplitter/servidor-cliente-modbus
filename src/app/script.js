angular.module('inicializador').controller('inicializadorServidorModbus', ['$scope', '$http', '$mdToast', function($scope, $http, $mdToast){

	$http.get('/servidorModbus')
	.then(function (response) {

		var socket = io();
		socket.on('dede', function (data) {
			$('.logs').append(
				'<h4 style="color:white; margin-left: 3%; margin-right: 3%; margin-bottom: -2%;">' +
					data.data + ' ' + data.log1 + ' ' + data.log2 + ', de ' + data.log3 + ' para ' + data.log4 +
				'</h4>'
			);
		});

		$scope.limparLog = function(){
			$('.logs').empty();
		};

		$scope.controleLigamentoServidorModbus = response.data;

		$scope.$watch(function(){ return $scope.controleLigamentoServidorModbus; }, function(n, o) {

			if(n != o){
				if(n)
					return $http.post('/servidorModbus')
					.then(function (response) {
						$mdToast.show(
							$mdToast.simple()
							.textContent(response.data)
							.hideDelay(3000)
						);
					});
				return $http.post('/servidorModbus/desliga')
				.then(function (response) {
					$mdToast.show(
						$mdToast.simple()
						.textContent(response.data)
						.hideDelay(3000)
					);
				});
			}
		});
	});
}]);
