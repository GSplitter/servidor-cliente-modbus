var express = require('express');
var router = express.Router();
var modbusEvent = require('modbus-event');
var modbus = require('jsmodbus');
var server;
var me;
var serverLigado = false;
var io = require('../bin/inicializador');
var dataEHora = require('../FreetimeJS/freetimejs').dataEHora;

var options = {
    debug : true,
    ip : '127.0.0.1',
    port : 502,
    id : 2
};

/* Rota que verifica estado do servidor modbus */
router.get('/', function(req, res){
	res.send(serverLigado);
});

/* Liga servidor modbus */
router.post('/', function(req, res) {

	server = modbus.server.tcp.complete({ port: 502, logEnabled: true, logLevel: 'debug' });
	serverLigado = true;

	me = modbusEvent(options);

	// Assign a listener event
	me.on('update', function(type, address, from, to){
		io.socket.emit('dede', { data: dataEHora(), log1: type, log2: address, log3: from, log4: to });
	});

	res.send("Servidor modbus ligado com sucesso!");
});

/* Desliga servidor modbus */
router.post('/desliga', function(req, res) {

	serverLigado = false;
	server.close();
	res.send("Servidor modbus desligado com sucesso!");
});

module.exports = router;
