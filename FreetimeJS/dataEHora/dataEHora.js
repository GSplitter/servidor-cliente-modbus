module.exports = function(){

	var data = new Date();
	var day = data.getDate(), month = data.getMonth() + 1, hours = data.getHours(), minutes = data.getMinutes(), seconds = data.getSeconds();

	day = day.toString();
	month = month.toString();
	hours = hours.toString();
	minutes = minutes.toString();
	seconds = seconds.toString();

	if(day.length < 2) day = '0' + day;
	if(month.length < 2) month = '0' + month;
	if(hours.length < 2) hours = '0' + hours;
	if(minutes.length < 2) minutes = '0' + minutes;
	if(seconds.length < 2) seconds = '0' + seconds;

	var newDate = '[' + day + '/' + month + '/' + data.getFullYear() + ' - ' + hours + ':' + minutes + ':' + seconds + ']';

	return newDate;

};
